<?php
try{
    $pdo = new PDO("mysql:host=localhost;dbname=my_chat", "root", "");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
    die("ERROR: Нет подключения. " . $e->getMessage());
}
try{
    $sql = "CREATE TABLE persons(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        first_name VARCHAR(30) NOT NULL,
        last_name VARCHAR(30) NOT NULL,
        email VARCHAR(70) NOT NULL UNIQUE
    )";    
    $pdo->exec($sql);
    echo "Таблица успешно создана.";
} catch(PDOException $e){
    die("ERROR: Не удалось выполнить $sql. " . $e->getMessage());
}
 
unset($pdo);
?>